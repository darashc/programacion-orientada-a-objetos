
package poo;

import java.util.Scanner;

public class POO {
    
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        Persona p1=new Persona();
        Persona p2=new Persona();
        
        p1.nombre="Persona 1";
        p1.puesto="Ordeño";
        p1.sueldo=500f;
        
        p2.nombre="Persona 2";
        p2.puesto="Carniceria";
        p2.sueldo=455f;
        
        System.out.println(p1.cuantoGana());
        System.out.println("Persona 1 gana "+p1.proporcionSueldo(p2)+" veces más que la Persona 2");
        System.out.println(p2.ajustaSueldoA(p1));
        
        Vaca v1=new Vaca();
        v1.nSerie=112133;
        v1.nombre="Vaca 1";
        v1.litros=new float[3];
        v1.peso=200;
        v1.asignaFuncion();
        
        Vaca v2=new Vaca();
        v2.nSerie=112132;
        v2.nombre="Vaca 2";
        v2.litros=new float[3];
        v2.peso=140;
        v2.asignaFuncion();
        
        Vaca v3=new Vaca();
        v3.nSerie=6254614;
        v3.nombre="Vaca 2";
        v3.litros=new float[3];
        v3.peso=30;
        v3.asignaFuncion();
        
        Vaca v4=new Vaca();
        v4.nSerie=8298754;
        v4.nombre="Vaca 2";
        v4.litros=new float[3];
        v4.peso=510;
        v4.asignaFuncion();
        
        Vaca[] todas={v1,v2};
        p1.quedarmeVacas(todas);
        
        Vaca[] todas2={v3,v4};
        p2.quedarmeVacas(todas2);
    }
    
}
