
package poo;


public class Persona {
    String nombre;
    String apellido;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca[] vacas;
    
    public String cuantoGana() {
        return "Gana "+this.sueldo+" €";
    }
    
    public float proporcionSueldo(Persona per) {
        return per.sueldo/this.sueldo;
    }
    
    public String ajustaSueldoA(Persona per) {
        switch (this.puesto) {
            case "Ordeño":
                if (per.puesto.equalsIgnoreCase("carniceria")) {
                    this.sueldo=(float) (per.sueldo*1.25);
                } else if (per.puesto.equalsIgnoreCase("gerencia")) {
                    this.sueldo=(float) (per.sueldo*0.75);
                }
                break;
            case "Carniceria":
                if (per.puesto.equalsIgnoreCase("ordeño")) {
                    this.sueldo=(float) (per.sueldo*0.75);
                } else if (per.puesto.equalsIgnoreCase("gerencia")) {
                    this.sueldo=(float) (per.sueldo*0.25);
                }
                break;
            case "Gerencia":
                if (per.puesto.equalsIgnoreCase("ordeño")) {
                    this.sueldo=(float) (per.sueldo*1.5);
                } else if (per.puesto.equalsIgnoreCase("carniceria")) {
                    this.sueldo=(float) (per.sueldo*1.75);
                }
                break;
        }
        
        return "Persona 1 gana: "+per.sueldo+"€\nPersona 2 gana: "+this.sueldo+"€";
    }
    
    public void quedarmeVacas(Vaca[] todas) {
        this.vacas=new Vaca[todas.length];
        int x=0;
        
        for (int i=0; i<todas.length; i++) {
            if (todas[i].funcion.equals(this.puesto)) {
                System.out.println(this.nombre+" se queda con la vaca de serie "+todas[i].nSerie);
                this.vacas[x]=todas[i];
                x++;
            } else {
                System.out.println(this.nombre+" no se queda con la vaca de serie "+todas[i].nSerie+". Razón: No correspnde los puestos de trabajo y la funciónd de la vaca.");
            }
        }
    }
    
}
